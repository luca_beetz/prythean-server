package prythean;

public final class GameConfig {

    /**
     * Maximum amount of active cards for each player
     */
    public final static int MAX_ACTIVE_CARDS = 6;

    public final static int MAX_PLAYER_MANA_START = 1;

    public final static int PLAYER_HEALTH = 30;

}