package prythean;

import prythean.server.GameServer;

public class Main {

    public static void main(String[] args) {
        System.out.println("Hello Prythean!");

        GameServer server = new GameServer(80);
        server.startServer();
    }

}
