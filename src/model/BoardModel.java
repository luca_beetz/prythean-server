package prythean.model;

import prythean.objects.player.Player;
import prythean.objects.player.PlayerEnum;
import prythean.objects.field.Field;
import prythean.model.TurnState;

public class BoardModel {

    /**
     * Player object for player A
     */
    private Player playerA;

    /**
     * Player object for player B
     */
    private Player playerB;

    /**
     * Game field
     */
    private Field field;

    /**
     * Current player's turn
     */
    private PlayerEnum currentPlayer;

    /**
     * State of the current turn
     */
    private TurnState turnState;

    public BoardModel(String playerNameA, String playerNameB) {
        this.playerA = new Player(playerNameA);
        this.playerB = new Player(playerNameB);

        this.field = field;

        currentPlayer = PlayerEnum.A;
        turnState = TurnState.Idle;
    }

    /**
     * Update observing views
     */
    public void updateObservers() {

    }
}
