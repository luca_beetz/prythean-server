package prythean.model;

public enum TurnState {
    Idle,
    Attacking,
}
