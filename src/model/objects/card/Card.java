package prythean.objects.card;

public abstract class Card {

    /**
     * Basic info about the Card
     */
    protected CardDetails details;

    public Card(CardDetails details) {
        this.details = details;
    }

}