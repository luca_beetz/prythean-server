package prythean.objects.card;

public class CardDetails {

    /**
     * Title of the card
     */
    private String title;

    /**
     * Path to the graphic for the card
     */
    private String imagePath;

    /**
     * Cost of mana to play this card
     */
    private int manaCost;

    public CardDetails(String title, String imagePath, int manaCost) {
        this.title = title;
        this.imagePath = imagePath;
        this.manaCost = manaCost;
    }

}