package prythean.objects.card;

public class Spell extends Card {

    /**
     * The damage this spell deals
     */
    private int damage;

    public Spell(CardDetails details, int damage) {
        super(details);

        this.damage = damage;
    }

}