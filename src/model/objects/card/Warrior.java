package prythean.objects.card;

public class Warrior extends Card {

    /**
     * Health points
     */
    private int health;

    /**
     * Damage dealt when attacking
     */
    private int strength;

    /**
     * Is card on the field
     */
    private boolean active;

    public Warrior(CardDetails details, int health, int strength, boolean active) {
        super(details);

        this.health = health;
        this.strength = strength;
        this.active = active;
    }

    public void attack(Warrior enemy) {
        enemy.reduceHealth(strength);
        health -= enemy.getStrength();
    }

    public int reduceHealth(int damage) {
        health -= damage;
        return health;
    }

    public int getStrength() {
        return strength;
    }

    public int getHealth() {
        return health;
    }
}