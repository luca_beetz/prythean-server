package prythean.objects.field;

import prythean.objects.stack.CardStack;
import prythean.objects.card.Card;
import prythean.objects.player.PlayerEnum;

import prythean.GameConfig;

public class Field {

    /**
     * Active cards for player A
     */
    private CardStack activeCardsA;

    /**
     * Active cards for player B
     */
    private CardStack activeCardsB;

    public Field(CardStack activeCardsA, CardStack activeCardsB) {
        this.activeCardsA = activeCardsA;
        this.activeCardsB = activeCardsB;
    }

    /**
     * Play a card for player A
     * @param card Card to play
     * @return Playing successful
     */
    public boolean playCardA(Card card) {
        return playCard(card, PlayerEnum.A);
    }

    /**
     * Play a card for player B
     * @param card Card to play
     * @return Playing successful
     */
    public boolean playCardB(Card card) {
        return playCard(card, PlayerEnum.B);
    }

    private boolean playCard(Card card, PlayerEnum player) {
        switch (player) {
            case A:
                if (activeCardsA.getCardCount() >= GameConfig.MAX_ACTIVE_CARDS) {
                    return false;
                }

                activeCardsA.addCard(card);
                break;

            case B:
                if (activeCardsB.getCardCount() >= GameConfig.MAX_ACTIVE_CARDS) {
                    return false;
                }

                activeCardsB.addCard(card);
                break;
        }

        return true;
    }

    /**
     * Get a card from the active cards of a player
     * @param index Index of the card
     * @param player Player
     * @return The selected Card
     */
    public Card getCard(int index, PlayerEnum player) {
        switch (player) {
            case A:
                return activeCardsA.getCard(index);

            case B:
                return activeCardsB.getCard(index);
        }

        return null;
    }

}