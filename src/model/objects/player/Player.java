package prythean.objects.player;

import prythean.GameConfig;
import prythean.objects.stack.CardStack;

public class Player {

    /**
     * Name of the player
     */
    private String name;

    /**
     * The player's deck
     */
    private CardStack deck;

    /**
     * The player's hand
     */
    private CardStack hand;

    /**
     * Maximum amount of mana
      */
    private int maxMana;

    /**
     * Available amount of mana
     */
    private int currentMana;

    /**
     * Health of the player
     */
    private int health;

    public Player(String name) {
        this.name = name;

        this.deck = new CardStack();
        this.hand = new CardStack();

        this.maxMana = GameConfig.MAX_PLAYER_MANA_START;
        this.currentMana = maxMana;

        this.health = GameConfig.PLAYER_HEALTH;
    }

    /**
     * Set the player's deck
     * @param deck Card deck
     */
    public void setCardDeck(CardStack deck) {
        this.deck = deck;
    }

    /**
     * Reduce health of the player
     * @param damage Damage to be done
     * @return New health
     */
    public int reduceHealth(int damage) {
        health -= damage;
        return health;
    }

    /**
     * Set current mana to maximal amount of mana
     */
    public void refillMana() {
        currentMana = maxMana;
    }

}