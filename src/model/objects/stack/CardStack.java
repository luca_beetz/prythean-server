package prythean.objects.stack;

import java.util.ArrayList;

import prythean.objects.card.Card;

public class CardStack {

    /**
     * List of cards in the stack
     */
    private ArrayList<Card> cards;

    public CardStack() {
        this.cards = new ArrayList<Card>();
    }

    public CardStack(ArrayList<Card> cards) {
        this.cards = cards;
    }

    /**
     * Get the size of the stack
     * @return Size of the stack
     */
    public int getCardCount() {
        return cards.size();
    }

    /**
     * Adds a card to the stack
     * @param card Card to add
     */
    public void addCard(Card card)  {
        cards.add(card);
    }

    public Card getCard(int index) {
        return cards.get(index);
    }

    /**
     * Removes a card from the stack
     * @param card Card to remove
     * @return Removed card
     */
    public Card removeCard(Card card) {
        int cardIndex = cards.indexOf(card);
        return cards.remove(cardIndex);
    }

}