package prythean.server;

import java.util.ArrayList;

public class Game {

    private static Game instance = null;

    private ArrayList<Session> sessionsPlayerA = new ArrayList<Session>();
    private ArrayList<Session> sessionsPlayerB = new ArrayList<Session>();

    public synchronized void joinPlayerA(Session session) {
        sessionsPlayerA.add(session);
    }

    public synchronized void leavePlayerA(Session session) {
        sessionsPlayerA.remove(session);
    }

    public synchronized void joinPlayerB(Session session) {
        sessionsPlayerB.add(session);
    }

    public synchronized void leavePlayerB(Session session) {
        sessionsPlayerB.remove(session);
    }

    public synchronized void sendDataPlayerA() {
        for (Session session : sessionsPlayerA) {

        }
    }

    public synchronized void sendDataPlayerB() {
        for (Session session : sessionsPlayerB) {

        }
    }

    public synchronized static Game getInstance() {
        if (instance == null) { instance = new Game(); }
        return instance;
    }

}