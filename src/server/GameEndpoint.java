package prythean.server;

import java.util.logging.Logger;
import java.util.logging.Level;

import prythean.server.Game;

@ServerEndpoint(value = "/game/{player}")
public class GameEndpoint {

    private Game room = Game.getInstance();

    private static final Logger logger = new Logger.getLogger(GameEndpoint.class.getSimpleName());

    @OnOpen
    public void onOpen(Session session, @PathParam("player") String playerCode) {
        loggger.log(Level.FINE, "New player joined: Player is: " + playerCode);

        if (playerCode == "A") {
            room.joinPlayerA(session);
        } else if (playerCode == "B") {
            room.joinPlayerB(session);
        }
    }

}