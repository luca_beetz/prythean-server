package prythean.server;

import prythean.controller.GameController;

public class GameServer {

    private int port;

    private GameController gameController;

    public GameServer(int port, GameController gameController) {
        this.port = port;
        this.gameController = gameController;
    }

    public void startServer() {
        System.out.println("Starting server on port: " + this.port);
    }

}